
import numpy as np
from numpy import exp, sqrt, pi, eye, kron
from scipy.linalg import toeplitz, svd
from PIL import Image

def blurring_kernel_1d(d,z):
    '''Blurring kernel value at distance d for blurring width z'''
    return exp( - (d*d) / (z*z) ) /  sqrt(pi) / z

def blurring_mat_1d(z,n):
    '''Build a 1d blurring kernel matrix for n pixels with blurring width z'''
    hist = np.array([blurring_kernel_1d(float(d),z) for d in range(n)])
    return toeplitz(hist)

def blurring_mat(z,shape):
    '''Build a 2d blurring kernel matrix for grayscale or rgb data'''
    A_x = blurring_mat_1d(z,shape[0])
    A_y = blurring_mat_1d(z,shape[1])
    A_1channel = kron(A_x, A_y)
    if len(shape) is 3:
        return kron(A_1channel, eye(shape[2]))
    else:
        return A_1channel

class blurring_op(object):
    def __init__(self, z, shape):
        self._A_x = blurring_mat_1d(z,shape[0])
        self._A_y = blurring_mat_1d(z,shape[1])
        self._U_x, self._S_x, self._V_x_T = svd(self._A_x)
        self._U_y, self._S_y, self._V_y_T = svd(self._A_y)
        self._S = np.outer(self._S_x,self._S_y)
        if len(shape) is 3:
            self._S = np.outer(self._S,np.ones(3)).reshape(shape)

    def __call__(self, X):
        '''Apply the blurring operation to an image'''
        temp1 = np.tensordot(self._A_x,X,([1],[0]))
        temp2 = np.tensordot(temp1,self._A_y,([1],[1]))
        if len(X.shape) is 3:
            return np.transpose(temp2,axes=[0,2,1])
        else:
            return temp2

    def solve(self, alpha, B, out=None, W = None):
        '''Solve min(||A X - B||^2 + alpha ||x||^2, where B is a blurry image
        and alpha is positive'''
        temp1 = np.tensordot(self._U_x,B,([0],[0]))
        temp2 = np.tensordot(temp1,self._U_y,([1],[0]))
        if len(B.shape) is 3:
            temp1 = np.transpose(temp2,axes=[0,2,1])
        else:
            temp1 = temp2
        temp1 = self._S * temp1
        if W is not None:
            temp2 = np.tensordot(self._U_x,W,([0],[0]))
            temp2 = np.tensordot(temp2,self._U_y,([1],[0]))
            if len(B.shape) is 3:
                temp2 = np.transpose(temp2,axes=[0,2,1])
            temp1 = temp1 + temp2
        temp1 = temp1 / (self._S * self._S + alpha)
        temp1 = np.tensordot(self._V_x_T,temp1,([0],[0]))
        temp2 = np.tensordot(temp1,self._V_y_T,([1],[0]))
        if len(B.shape) is 3:
            temp1 = np.transpose(temp2,axes=[0,2,1])
        else:
            temp1 = temp2
        if out:
            out = temp1
        else:
            return temp1

def numpy_wikicommons_image(name,grayscale = False):
    from urllib.request import urlopen
    from io import BytesIO

    URL = "https://upload.wikimedia.org/wikipedia/commons/" + name

    with urlopen(URL) as url:
        f = BytesIO(url.read())
    a = np.array(Image.open(f)).astype('Float64')

    if a.shape[2] is 4:
        for j in range(3):
            a[...,j] = (a[...,3] / 255.) * a[...,j] + (255. - a[...,3])
        a = a[...,0:3]
    if grayscale:
        # https://stackoverflow.com/questions/12201577/how-can-i-convert-an-rgb-image-into-grayscale-in-python
        a = a[...,0] * 0.299 + a[...,1] * 0.587 + a[...,2] * 0.114
    return a/255.

def rgb_show(rgb):
    uint_rgb = rgb.astype('uint8')
    im = Image.fromarray(uint_rgb)
    im.show()
