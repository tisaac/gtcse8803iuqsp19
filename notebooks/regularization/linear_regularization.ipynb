{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Regularization of linear least squares"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Review\n",
    "\n",
    "**Recall from notes on [ill-posedness and ill-conditioning](../conditioning/conditioning.ipynb):**\n",
    "\n",
    "Suppose $F:\\mathbb{R}^m \\to \\mathbb{R}^n$ has algebraic rank $r\\leq \\min(m,n)$.  Let the skinny SVD of $F$ be\n",
    "$$F = U \\Sigma V^T,\\quad U\\in \\mathbb{R}^{n\\times r}, \\quad \\Sigma\\in\\mathbb{R}^{r\\times r}, \\quad V \\in \\Sigma\\mathbb{R}^{m\\times r}.$$\n",
    "\n",
    "Then the pseudoinverse of $F$ is given by\n",
    "$$F^+ = V \\Sigma^{-1} U^T.$$\n",
    "\n",
    "We can have versions of our perturbation results for the pseudoinverse that rely on an analogous generalized condition number: $$\\kappa^+_2(F) = \\|F\\|_2 \\|F^+\\|_2.$$\n",
    "\n",
    "In the case of a perturbed righthand side, the result is only changed by being bounded by the size of the perturbation relative to the *projection* of the right-hand side onto the range of $F$:\n",
    "\n",
    "**(RHS Perturbation)**\n",
    "If $x = F^+ d$ and $\\tilde{x} = F^+(d + \\delta)$, then\n",
    "$$\\frac{\\|x - \\tilde{x}\\|}{\\|x\\|} \\leq\n",
    "\\kappa_2^+(F)\\frac{\\|\\delta\\|}{\\|\\Pi_F d\\|},$$\n",
    "\n",
    "where $\\Pi_F$ is the orthogonal projector onto the range of $F$.  Thus, even if $\\kappa_2^+(F)$ is well-conditioned, we have to consider the size of the perturbation $\\|\\delta\\|$ relative to its projection: if $d$ is nearly orthogonal to the range of $F$, then there are problems.\n",
    "\n",
    "In the case of a perturbed operator, however, we can only bound components of the error:\n",
    "\n",
    "**(Operator Perturbation)**\n",
    "Suppose $\\tilde{F} + \\Phi$ is non-singular.  If $x = F^+ d$ and $\\tilde{x} = \\tilde{F}^+ d$ and $\\tilde{F} = F + \\Phi$, then\n",
    "\n",
    "$$\\frac{\\|\\Pi_{F^+}(x - \\tilde{x})\\|}{\\|\\tilde{x}\\|} \\leq\n",
    "\\kappa_2^+(F)\\left(\\frac{\\|\\Pi_{\\tilde{F}}^\\perp d\\|}{\\|\\Pi_{\\tilde{F}} d\\|} + \n",
    "\\frac{\\|\\Pi_{\\tilde{F}}^\\perp d\\|}{\\|\\Pi_{\\tilde{F}} d\\|}\\frac{\\|\\Phi\\|}{\\|F\\|} +\n",
    "\\frac{\\|\\Phi\\|}{\\|F\\|}\\right).$$\n",
    "\n",
    "So there error depends not only on the size of the perturbation $\\|\\Phi\\|$, but\n",
    "on how big the two components of $d$, $\\Pi_{\\tilde{F}}d$ and $\\Pi_{\\tilde{F}}^\\perp d$ are. \n",
    "\n",
    "In the case where the perturbed operator $\\tilde{F}^+$ is non-singular, this simplifies to\n",
    "\n",
    "$$\\frac{\\|\\Pi_{F^+}(x - \\tilde{x})\\|}{\\|\\tilde{x}\\|} \\leq\n",
    "\\kappa_2^+(F)\\left(\\frac{\\|\\Phi\\|}{\\|F\\|}\\right).$$\n",
    "\n",
    "In the case where the original operator is non-singular, this simplifies to\n",
    "\n",
    "$$\\frac{\\|x - \\tilde{x}\\|}{\\|\\tilde{x}\\|} \\leq\n",
    "\\kappa_2(F)\\left(\\frac{\\|\\Pi_{\\tilde{F}}^\\perp d\\|}{\\|\\Pi_{\\tilde{F}} d\\|} + \n",
    "\\frac{\\|\\Pi_{\\tilde{F}}^\\perp d\\|}{\\|\\Pi_{\\tilde{F}} d\\|}\\frac{\\|\\Phi\\|}{\\|F\\|} +\n",
    "\\frac{\\|\\Phi\\|}{\\|F\\|}\\right).$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Example: Deblurring\n",
    "\n",
    "To demonstrate these points, let's consider the example where $F$ is a blurring kernel applied to an image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from blurring_example import numpy_wikicommons_image, blurring_mat, blurring_op\n",
    "from numpy.random import randn\n",
    "from scipy.linalg import svd, solve\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "plt.set_cmap('gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's get an image (copyright Rodolfoguimaraes [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0) from [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Stinglessbee.png))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = numpy_wikicommons_image('d/db/Stinglessbee.png', grayscale=True)\n",
    "plt.imshow(X)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's get the matrix for a blurring operator on images of this size:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "F = blurring_mat(2, X.shape)\n",
    "N = F.shape[0]\n",
    "plt.imshow(F)\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's directly compute the SVD of this matrix (might take a few minutes, go have a snack)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "U, S, VT = svd(F)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.semilogy(S)\n",
    "plt.xlabel('Index k')\n",
    "plt.title('Singular values of F')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see singular values ranging between ~1 and ~$10^{-8}$, meaning that $F$ is not singular by double-precision standards ($\\epsilon_{\\text{mach}} \\approx 10^{-16}$), but it is certainly ill-conditioned."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our matrix $F$ operates on the pixel values $X$, but that is stored as a 2D array, rather than a vector: for convenience, let's define a function that applies $F$ but keeps the image shape:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dot_img(F, X):\n",
    "    return F.dot(X.flatten()).reshape(X.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is the result of blurring our image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "B = dot_img(F,X)\n",
    "plt.imshow(B)\n",
    "plt.title('Blurred image $B = FX$')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have an analog of `dot_img` but for solving a system (taking advantage of the case where we have already computed the SVD, which we have):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_img(F,B,USV=None):\n",
    "    if USV:\n",
    "        U, S, VT = (USV[0], USV[1], USV[2])\n",
    "        return dot_img(VT.T,dot_img(U.T,B) / S.reshape(B.shape))\n",
    "    else:\n",
    "        return solve(F.B.flatten()).reshape(B.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, here is the result of our attempt to invert $F$: to deblur an image when we know exactly what the blurring process was:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_rec = solve_img(F,B,USV=(U,S,VT))\n",
    "\n",
    "plt.imshow(X_rec)\n",
    "plt.title('Deblurred image $X_{rec} = F^{-1}B$')\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Look's pretty good!  But what if we have a perturbed right hand side $B$, due to factors out of our control, such as noise in the imaging process?  Let's add 5% noise to the blurred image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "B_noisy = B * (1. + 0.05 * randn(B.shape[0],B.shape[1]))\n",
    "plt.subplot(1,2,1)\n",
    "plt.imshow(B)\n",
    "plt.title(\"$B$\")\n",
    "plt.subplot(1,2,2)\n",
    "plt.imshow(B_noisy)\n",
    "plt.title(\"$B_{noisy} = B +$ 5% noise\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What does our recovered original image look like if we try to deblur the noisy image exactly?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_rec_noisy = solve_img(F,B_noisy,USV=(U,S,VT))\n",
    "plt.imshow(X_rec_noisy)\n",
    "plt.colorbar()\n",
    "plt.title(\"$X_{rec noisy} = F^{-1} B_{noisy}$\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice the scale in the colorbar: if 0 is black and 1 is white, this image is mostly blacker than black and whiter than white.  While that's not possible, we can verify that this \"picture\" blurs to the noisy image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "B_rec_noisy = dot_img(F,X_rec_noisy)\n",
    "plt.imshow(B_rec_noisy)\n",
    "plt.title(\"$B_{recnoisy} = F X_{recnoisy}$\")\n",
    "plt.show()\n",
    "diff = B_rec_noisy - B_noisy\n",
    "print(\"||F X_rec_noisy - B_noisy||/||B_noisy|| = {}\".format(np.linalg.norm(diff)/np.linalg.norm(B_noisy)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What's happening here?\n",
    "\n",
    "Let's take a \"blurred image\" $Z$ that is just noise and convert it into the $U$ basis: the orthogonal image vectors that describe the range of $F$ in order of decreasing singular values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Z = randn(B.shape[0],B.shape[1])\n",
    "UTZ = U.T.dot(Z.flatten())\n",
    "UTB = U.T.dot(B.flatten())\n",
    "plt.semilogy(abs(UTB),label='blurred image B')\n",
    "plt.semilogy(abs(UTZ),label='noise Z')\n",
    "plt.xlabel('singular value index k')\n",
    "plt.title('$|u_k^T \\{Z,B\\}|$')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The true image has very small components at the end of the spectrum, which makes sense: $u_k^T B$ = \n",
    "$u_k^T U \\Sigma V^T X = \\sigma_k v_k^T X$.  Thus however big $X$ was in the $v_k$ direction, it was shrunk by $\\sigma_k$.  So unless $|v_k^T X|$ grows a lot with $k$ to counteract the decrease of $\\sigma_k$, we expect \n",
    "$|u_k^T B|$ to decrease.\n",
    "\n",
    "Does $|v_k^T X|$ grow with $k$?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "VTX = VT.dot(X.flatten())\n",
    "plt.semilogy(abs(VTX))\n",
    "plt.xlabel('singular value index k')\n",
    "plt.title('$|v_k^T X|$')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In fact we see the opposite: the size of the components of the true solution trends down as $k$ increases.\n",
    "\n",
    "The components of $|u_k^T Z|$, on the other hand, exhibit no real trend in $k$, which means that towards the far end of the spectrum, noise dominates the originally blurring image $B$: $$u_k^T(B_{\\text{noisy}}) = u_k^T(B + 0.05  Z) \\approx 0.05 u_k^T Z.$$  This means that in the inverted solution $X_{\\text{rec}}$, \n",
    "$v_k^T X_{\\text{rec noisy}} \\approx 0.05 \\sigma_k^{-1} u_k^T Z$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "VTX_rec_noisy = VT.dot(X_rec_noisy.flatten())\n",
    "plt.semilogy(abs(VTX),label='$X$')\n",
    "plt.semilogy(abs(VTX_rec_noisy),label='$X_{rec noisy}$')\n",
    "plt.xlabel('singular value index k')\n",
    "plt.title('$|v_k^T X|$')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. The truncated SVD solution\n",
    "\n",
    "\n",
    "Since noise is dominating at the small end of the spectrum, one way to solve $Fx=d$ is to truncate the inverse operator after some fixed number of singular values $k$:\n",
    "\n",
    "$$x_k^+ = \\sum_{i = 1}^k \\sigma_i^{-1} v_i u_i^T d.$$\n",
    "\n",
    "This has been implemented below.  Choose a value of $k$ that you think is the best solution to the deblurring problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "UB = U.T.dot(B_noisy.flatten())\n",
    "SUB = UB / S\n",
    "def solve_trunc(k):\n",
    "    return SUB[0:k].dot(VT[0:k,:]).reshape(X.shape)\n",
    "\n",
    "def solve_trunc_viz(k):\n",
    "    X_k = solve_trunc(k)\n",
    "    plt.imshow(X_k)\n",
    "    plt.colorbar()\n",
    "\n",
    "from ipywidgets import interact, IntSlider, FloatSlider\n",
    "interact(solve_trunc_viz, k=IntSlider(value=len(S)//2,min=1,max=F.shape[0]-1,continuous_update=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How should we choose $k$ for truncation?\n",
    "\n",
    "For deblurring, we are fortunate that the measure of the quality of a solution is literally the fabled \"eye norm.\"  For general inverse problems, the solution space where $x$ resides may not visualize well, or we might be worried about using a criterion that is so subjective.  So what is a more rigorous way to choose $k$?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Method 1: The \"L-curve\" criterion\n",
    "\n",
    "Notice that the definition of $x_k$ is the sum of scaled $v_i$ vectors from the SVD of $F$, which are orthogonal vectors.  Thus, the norm $||x_k||$ must be monotonically increasing with $k$.\n",
    "\n",
    "What about the residual, $F x_k - d$? Letting $V_k, U_k$ be the first $k$ vectors of $U$ and $V$, and $\\Sigma_k$ being the associated singular values, we have\n",
    "\n",
    "$$Fx_k - d = U \\Sigma V^T x_k - d = U\\Sigma V^T V_k \\Sigma_k^{-1} U_k^T d - UU_k^T d = U(U - U_k)^T d =(I - U_k U_k^T) d.$$\n",
    "\n",
    "This means that the residual is the projection of $d$ onto the complement of $U_k$.  This must mean that\n",
    "$\\|F x_k - d\\|$ is monotonically decreasing with $k$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rnorms = np.array([np.linalg.norm(UB[k:]) for k in range(len(UB))])\n",
    "xnorms = np.array([np.linalg.norm(SUB[0:k]) for k in range(len(SUB))])\n",
    "\n",
    "plt.semilogy(rnorms / rnorms[0],label=\"relative residual norm $\\|Fx_k - d\\|/\\|d\\|$\")\n",
    "plt.semilogy(xnorms / xnorms[-1],label=\"relative solution norm $\\|x_k\\|/\\|x\\|$\")\n",
    "plt.xlabel('Truncation index k')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Clearly we should try to minimize some combination of these two values.  We could add them, trying to minimize\n",
    "$a\\|x_k\\| + b\\|Fx_k - d\\|$, but the value of $k$ that minimized the sum would be highly dependent on the scaling parameters $a$ and $b$.\n",
    "\n",
    "The product $\\|x_k\\|\\|Fx_k - d\\|$, however, will have the same minimizer regardless of their scales relative to each other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prod = xnorms * rnorms\n",
    "plt.semilogy(prod)\n",
    "k_min = np.argmin(prod[1:-2]) + 1 # excluding endpoints where product is zero\n",
    "plt.semilogy(k_min, prod[k_min],'rx')\n",
    "plt.annotate('k = {}'.format(k_min),(k_min, prod[k_min]))\n",
    "plt.title('$\\|X_k\\| \\|F X_k - B_{noisy}\\|$')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We ignore the sharp lines at the endpoints, which correspond to choosing $k = 0$ and thus $x_k = 0$, and\n",
    "$k = N$, resulting in $x_k = x^+ = F^+ d$.  Between those ends, we see that the product is exactly minimized at one value of $k$, but there is a basin of values which are close to the optimum.  Hopefully, your subjective choice of the best $k$ for the deblurring problem corresponds to one of these values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A common way to visualize this information is not to plot the product, but to use them as axes in a logarithmic plot.  The values of $k$ that are nearly optimal cluster at the bend in a curve that is roughly $L$-shaped,\n",
    "hence the name \"L-curve criterion\" for this method of choosing $k$.\n",
    "\n",
    "It can be expensive to search for the optimal $k$: this visualization starkly demonstrates when you have chosen a value of $k$ that is good enough."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def truncated_norms(k):\n",
    "    X_norm = xnorms[k]\n",
    "    R_norm = rnorms[k]\n",
    "    plt.loglog(xnorms, rnorms)\n",
    "    plt.loglog(X_norm,R_norm,'rx')\n",
    "    plt.annotate('k = {}'.format(k),(X_norm,R_norm))\n",
    "    plt.xlabel('||X_k||')\n",
    "    plt.ylabel('||FX_k - B_noisy||')\n",
    "    \n",
    "interact(truncated_norms, k=IntSlider(value=len(S)//2,min=1,max=F.shape[0]-1,continuous_update=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the L-curve optimum is a value not only of the operator $F$, but of the right hand side.  In particular,\n",
    "if different noisy processes are used, then different optimal values are arrived at.  We show this below with\n",
    "right hand sides with 1%, 5%, and 25% noise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_n = []\n",
    "for noise in [0.01,0.05,0.25]:\n",
    "    B_n = B * (1. + noise* randn(B.shape[0],B.shape[1]))\n",
    "    UB_n = U.T.dot(B_n.flatten())\n",
    "    rnorms_n = np.array([np.linalg.norm(UB_n[k:]) for k in range(len(UB_n))])\n",
    "    SUB_n = UB_n / S\n",
    "    xnorms_n = np.array([np.linalg.norm(SUB_n[0:k]) for k in range(len(SUB_n))])\n",
    "    prod = rnorms_n * xnorms_n / (rnorms_n[0] * xnorms_n[-1])\n",
    "    plt.loglog(xnorms_n,rnorms_n,label=str(noise))\n",
    "    k_min = np.argmin(prod[1:-2]) + 1\n",
    "    X_n.append(solve_trunc(k_min))\n",
    "    plt.loglog(xnorms_n[k_min],rnorms_n[k_min],'rx')\n",
    "    plt.annotate('k = {}'.format(k_min),(xnorms_n[k_min],rnorms_n[k_min]))\n",
    "plt.legend()\n",
    "plt.xlabel('||X_k||')\n",
    "plt.ylabel('||F X_k - B_n||')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What trends do we notice?\n",
    "\n",
    "- The optimal value of $k$ tends to decrease with increasing noise\n",
    "- The change in the solution norm $\\|X_k\\|$ of the optimal $k$ is relatively insensitive to increasing noise\n",
    "- The change in the residual norm $\\|F X_k - B_n\\|$ is quite sensitive to increasing noise.  This makes sense, because as noise increases, it swamps more and more components of the right hand side, giving us less information to go on.\n",
    "\n",
    "Here is what these optimal solutions look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(X_n[0])\n",
    "plt.title('X_rec, 1% noise')\n",
    "plt.show()\n",
    "plt.imshow(X_n[1])\n",
    "plt.title('X_rec, 5% noise')\n",
    "plt.show()\n",
    "plt.imshow(X_n[2])\n",
    "plt.title('X_rec, 10% noise')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Method 2: Morozov's Discrepancy Principle\n",
    "\n",
    "The $L$-curve criterion is nice because, even though the choice of $k$ depends on the noise, the source, nature, and magnitude of the noise are not required for the calculation.\n",
    "\n",
    "But suppose we do know, something about the noise.  In our case, we know that\n",
    "$B_{noisy} = B(I + \\text{diag}(z))$, where $z$ is normally distributed noise, $z\\sim N(0,0.01)$.\n",
    "This means that the expected value of $\\|B_{noisy} - B\\|^2$ is $\\|B\\|^2 0.05^2$, which is approximately\n",
    "$\\|B_{noisy}\\|^2 0.05^2$.  Using the triangle inequality, we have\n",
    "\n",
    "$$\\|F X_k - B\\| \\leq \\| F X_k - B_{noisy}\\| + \\| B_{noisy} - B\\| \\approx\n",
    "\\|F X_k - B_{noisy} \\| + 0.05 \\|B_{noisy}\\|.$$\n",
    "\n",
    "Given the uncertainty in the value of the $B$, there is no use in making the residual smaller than the expected size of the noise.  Thus, we should choose the smallest solution such that $$\\|F X_k - B_{noise} \\| \\leq\n",
    "0.05 \\|B_{noisy}\\|.$$  More generally, for any noise for which we can get an expected magnitude, we should choose the smallest $k$ such that\n",
    "\n",
    "$$\\|\\text{residual}\\| \\leq E(\\|\\text{noise}\\|).$$\n",
    "\n",
    "This is known as *Morozov's Discrepancy Principle*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Bnorm = np.linalg.norm(B_noisy) # Use the measured norm as a proxy for the true norm\n",
    "noise = 0.05 * Bnorm\n",
    "\n",
    "print(\"||B|| ~ {}\".format(Bnorm))\n",
    "print(\"||noise|| ~ {}\".format(noise))\n",
    "from bisect import bisect_left\n",
    "\n",
    "k_morozov = N - bisect_left(rnorms[::-1], noise)\n",
    "print(\"k_morozov = {}\".format(k_morozov))\n",
    "X_morozov = solve_trunc(k_morozov)\n",
    "B_morozov = dot_img(F, X_morozov)\n",
    "r_morozov = np.linalg.norm((B_morozov - B_noisy).flatten())\n",
    "plt.imshow(B_morozov - B_noisy)\n",
    "plt.colorbar()\n",
    "print(\"||F X_morozov - B_noisy|| = {}\".format(r_morozov))\n",
    "plt.imshow(X_morozov)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. The Tikhonov regularized solution\n",
    "\n",
    "We saw that the truncated SVD solution has some nice properties, but the SVD is expensive to compute, even on a small example problem.  For bigger problems, computing the full SVD is completely out of the question:\n",
    "\n",
    "(image copyright Bombman356 [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0) from [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Yellow_jacket_wasp.jpg))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = numpy_wikicommons_image('7/7f/Yellow_jacket_wasp.jpg')\n",
    "plt.imshow(X)\n",
    "plt.title('Bigger Image')\n",
    "plt.show()\n",
    "N = X.shape[0] * X.shape[1] * X.shape[2]\n",
    "print(\"N = {}\".format(N))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This file is about 100 times larger than the previous one, meaning that direct computation of the full SVD would take about 1 million times as long.  In fact, we should probably avoid forming the full blurring operator $F$ as a matrix as well: we will import some code that *applies* F to an image without forming the full matrix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "F = blurring_op(10,X.shape)\n",
    "B = F(X)\n",
    "plt.imshow(B)\n",
    "plt.title(\"Big blurred image\")\n",
    "plt.show()\n",
    "B_noisy = B * (1. + 0.05 * randn(B.shape[0],B.shape[1],B.shape[2]))\n",
    "plt.imshow(B_noisy)\n",
    "plt.title(\"Big blurred image + 5% noise\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the notes on ill-posedness and ill-conditioning, we saw one definition of the Moore-Penrose pseudoinverse as\n",
    "\n",
    "$$F^+ = \\lim_{\\alpha \\searrow 0} (\\alpha I + F^T F)^{-1} F^T.$$\n",
    "\n",
    "Let $F_{\\alpha}^+ = (\\alpha I + F^T F)^{-1} F^T$. It is an exercise to see that $x_\\alpha := F_\\alpha^+ d$ solve an augmented least squares system,\n",
    "\n",
    "$$x_\\alpha = \\text{argmin}_x \\frac{1}{2}\\left\\| \\begin{bmatrix} F \\\\ \\sqrt{\\alpha} I \\end{bmatrix} x - \n",
    "\\begin{bmatrix} d \\\\ 0 \\end{bmatrix}\\right\\|^2.$$\n",
    "\n",
    "A little bit of linear algebra will also show that the above optimization problem is equivalent to\n",
    "\n",
    "$$x_\\alpha = \\text{argmin}_x \\frac{1}{2}\\| F x - d \\|^2 + \\frac{\\alpha}{2} \\|x\\|^2.$$\n",
    "\n",
    "This is sometimes called the Tikhonov regularized solution of the inverse problem.\n",
    "\n",
    "For the truncated SVD and we discussed different truncation indices providing a trade off between\n",
    "the size of the solution and the size of the residual. Here we see that the $\\alpha$-regularized solution is the result of trying to minimize both simultaneously by summing the terms, with $\\alpha$ serving as the exchange rate: the relative importance of residual norm to solution norm.\n",
    "\n",
    "Although we are not calculating the SVD, the SVD of $F$ tells us something about the form of the regularized solution:\n",
    "\n",
    "$$ x_\\alpha = (\\alpha I + F^T F)^{-1} F^T d = (\\alpha I + V \\Sigma^2 V^T)^{-1} V \\Sigma U^T d = \\sum_{i=1}^N \\frac{\\sigma_i}{\\alpha + \\sigma_i^2} u_i^T d.$$\n",
    "\n",
    "We note that if $\\alpha \\ll \\sigma_i^2$, then $\\sigma_i / (\\alpha + \\sigma_i^2) \\approx \\sigma_i^{-1}$,\n",
    "which would mean $v_i^T x_\\alpha \\approx v_i^T x_k^+$ if $i < k$.\n",
    "\n",
    "Likewise, if $\\alpha \\gg \\sigma_i^2$, then $\\sigma_i / (\\alpha + \\sigma_i^2) \\approx 0$, and\n",
    "thus $v_i^T x_\\alpha \\approx v_i^T x_k^+$ if $i > k$.\n",
    "\n",
    "So for both the components of the solution associated with both the largest and smallest singular values of $F$,\n",
    "the $\\alpha$-regularized solution looks very similar to the truncated SVD solution."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Properties of Tikhonov Regularization\n",
    "\n",
    "Tikhonov regularization can be used for nonlinear inverse problems (more on that in other slides), but for linear inverse problems, Tikhonov regularization can be shown to have the same monotonic properties as the SVD truncated regularization (but with increasing $\\alpha$ being like decreasing $k$).\n",
    "\n",
    "**Monotonic decrease in solution norm**: $\\|x_\\alpha\\|$ is decreasing with $\\alpha$.\n",
    "\n",
    "**Monotonic increase in residual norm**: $\\|F x_\\alpha - d\\|$ is increasing with $\\alpha$.\n",
    "\n",
    "Therefore, we can use the same strategies that we used to choose the truncation index $k$ to choose the regularizaton value $\\alpha$.\n",
    "\n",
    "My blurring operation comes equipped with a relatively fast method to solve the $\\alpha$ regularized inverse problem.  First let's take a look at computing a value of $\\alpha$ from the L-curve."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def alpha_norms(alpha):\n",
    "    X_alpha = F.solve(alpha, B_noisy)\n",
    "    x_norm = np.linalg.norm(X_alpha)\n",
    "    r_norm = np.linalg.norm(F(X_alpha) - B_noisy)\n",
    "    return (x_norm,r_norm)\n",
    "\n",
    "x_norms = []\n",
    "r_norms = []\n",
    "for alpha in [1.e-9,1.e-8,1.e-7,1.e-6,1.e-5,1.e-4,0.001,0.01,0.1,1.0,10.0,100.0]:\n",
    "    x_norm, r_norm = alpha_norms(alpha)\n",
    "    x_norms.append(x_norm)\n",
    "    r_norms.append(r_norm)\n",
    "\n",
    "def alpha_norms_interact(logalpha):\n",
    "    x_norm, r_norm = alpha_norms(10**logalpha)\n",
    "    plt.loglog(x_norms, r_norms,'b')\n",
    "    plt.loglog(x_norm,r_norm,'rx')\n",
    "    plt.xlabel('||X_alpha||')\n",
    "    plt.ylabel('||F X_alpha - B_noisy||')\n",
    "    plt.annotate('alpha = 10**{}'.format(logalpha),(x_norm, r_norm))\n",
    "    plt.show()\n",
    "    \n",
    "from ipywidgets import FloatSlider\n",
    "    \n",
    "interact(alpha_norms_interact,logalpha=FloatSlider(value=0.,min=-9,max=16,continuous_update=False))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def interact_alpha(logalpha):\n",
    "    plt.imshow(F.solve(10**logalpha,B_noisy))\n",
    "    plt.title(\"$X_{{alpha}}$ for $alpha = {}$\".format(10**logalpha))\n",
    "    plt.show()\n",
    "\n",
    "interact(interact_alpha,logalpha=FloatSlider(value=0.,min=-9.,max=1.,continuous_update=False))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can use Morozov's discrepancy principle to search for the largest value of $\\alpha$ such that\n",
    "$\\|F x_\\alpha - d\\| \\leq E(\\|\\text{noise}\\|)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Bnorm = np.linalg.norm(B_noisy) # Use the measured norm as a proxy for the true norm\n",
    "noise = 0.05 * Bnorm\n",
    "\n",
    "print(\"||B|| ~ {}\".format(Bnorm))\n",
    "print(\"||noise|| ~ {}\".format(noise))\n",
    "\n",
    "alpha_min = 1.e-6\n",
    "alpha_max = 1.e1\n",
    "\n",
    "while np.log10(alpha_max) - np.log10(alpha_min) > 0.1:\n",
    "    alpha = 10**((np.log10(alpha_max) + np.log10(alpha_min))/2.)\n",
    "    X_alpha = F.solve(alpha,B_noisy)\n",
    "    r_alpha = np.linalg.norm(F(X_alpha) - B_noisy)\n",
    "    if r_alpha < noise:\n",
    "        alpha_min = alpha\n",
    "    else:\n",
    "        alpha_max = alpha\n",
    "print(\"alpha_morozov = {}\".format(alpha))\n",
    "print(\"||F X_alpha - B_noisy|| = {}\".format(r_alpha))\n",
    "plt.imshow(X_alpha)\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
