# CSE 8803 (Spring 2019, 3 Credit Hours) | Computational Inverse Problems and Uncertainty Quantification (IUQ)

## Contact

|          | **Instructor**                                      |
| ---      | :------------------                                 |
| *name*   | Toby Isaac                                          |
| *email*  | [tisaac@cc.gatech.edu](mailto:tisaac@cc.gatech.edu) |
| *phone*  | [(404)385-5970](tel:+14043855970)                   |
| *office* | [KACB 1302](https://goo.gl/38x6QB)                  |

**Office Hours:** Fridays, 9:00--10:00 a.m., [KACB 1302](https://goo.gl/38x6QB)

## Lectures

Tuesdays & Thursdays, 12:00--1:15 p.m., [Cherry Emerson 322](https://map.concept3d.com/?id=82#!m/10996)

## Tentative Schedule

| Week  | Dates                | Topics                                                      | Reading |
| ----: | -----                | ------                                                      | ------- |
| 1     | Jan. 8 & 10          | - [Introduction][s0]                                        | Prof. Stadler's [introductory notes][s0r] |
|       |                      | **Module 1: inverse problems and optimization**             | Prof. Embree's [chapter 4][s1r] |
|       |                      | - [Conditioning of linear problems][s1]                     |         |
| 2     | Jan. 15 & 17         | - Well-posedness, ill-posedness, & ill-conditioning         |         |
|       |                      | - Regularization of linear least squares                    | Prof. Embree's [chapter 7][s1r] |
|       |                      | - Regularization with different norms                       | g2s3: [TV reguarlization][s2r] |
| 3     | Jan. 22 & 24         | - Numerical optimization I: Newton and friends              | Prof. Stadler's [numerical optimization notes][s3r] |
| 4     | Jan. 29 & 31         | - Variational calculus, first- and second-order sensitivity |         |
|       |                      | - Adjoint methods                                           |         |
| 5     | Feb. 5 & 7           | - Numerical optimization II: Reduced space and full space   | Prof. Boyd's [ADMM slides][s4r] |
|       |                      |                                                             | [Kim, Koh, et al. 2007][s5r] |
|       |                      |                                                             | [Gondzio 2011][s6r] |
|       |                      | - Numerical optimization III: Method of multipliers & Interior Point Methods |         |
| 6     | Feb. 12 & 14         | - ~~Unregularized approaches to inverse problems~~          |         |
|       |                      | - ~~Miscellany I: Checkpointing~~                           |         |
|       |                      | - Numerical optimizavion IV: Stochastic gradient descent    | [Bottou 2010][s7r] |
|       |                      | **Module 2: Bayesian Inference**                            |         |
| 7     | Feb. ~~19~~ & 21     | - Bayesian statistical framework                            |         |
| 8     | Feb. ~~26~~ & ~~28~~ | *SIAM CSE*                                                  |         |
| 9     | Mar. 5 & 7           | - Linear-Gaussian Bayesian inverse problems                 |         |
|       |                      | - Covariance / Hessian construction and approximation       |         |
|       |                      | - Randomized linear algebra                                 |         |
| 10    | Mar. 12 & 14         | - Inference for prediction                                  |         |
|       |                      | - Miscellany II: Bayesian brittleness                       |         |
|       |                      | **Module 3: Markov Chains**                                 |         |
| 11    | Mar. ~~19~~ & ~~21~~ | *Spring Break*                                              |         |
| 12    | Mar. 26 & 28         | - Markov Chain Introduction                                 |         |
| 13    | Apr. 2 & 4           | - Numerical Integration I: Metropolis-Hastings              |         |
| 14    | Apr. 9 & 11          | - Numerical Integration II: Importance Sampling             |         |
| 15    | Apr. 16 & 18         | - Miscellany III: Non-Bayesian uncertainty quantification   |         |
| 16    | Apr. 23              | - ???                                                       |         |
|       |                      |                                                             |         |

[s0]: notebooks/introduction/introduction.ipynb
[s0r]: https://gatech.instructure.com/courses/70924/files?preview=2689505
[s1]: notebooks/conditioning/conditioning.ipynb
[s1r]: https://gatech.instructure.com/courses/70924/files?preview=3147499
[s2r]: http://g2s3.com/labs/notebooks/ImageDenoising.html
[s3r]: https://gatech.instructure.com/courses/70924/files?preview=2862751
[s4r]: https://web.stanford.edu/~boyd/papers/pdf/admm_slides.pdf
[s5r]: https://web.stanford.edu/~boyd/papers/pdf/l1_ls.pdf
[s6r]: https://www.maths.ed.ac.uk/~gondzio/REF/gondzioIPMsXXV.pdf
[s7r]: http://khalilghorbal.info/assets/spa/papers/ML_GradDescent.pdf

## Description

In many fields of science we develop descriptive models of reality; in
scientific computing, we use (or approximate) those models to make predictions.

But there are many models to choose from: all of them wrong, and only some of
them useful.  How do we choose which models to use for our predictions?  Ideally, 
our choice should be driven by comparing the model to what we can observe.

If the process of making a prediction from a model is the *forward* modeling problem,
then choosing a model from observations is the *inverse* modeling problem.
This course will introduce computational methods for solving the inverse problem.
The focus will be on cases where:

- The models, coming from domains of science with expert knowledge, are
  potentially large and expensive to evaluate
- The choice between models can be characterized by the choice between
  different values of model parameters, with focus on large
  (even infinite-dimensional!) parameter spaces

This course will introduce computational methods for solving the inverse problem
and software tools that implement those methods.

In addition to the inverse problem of selecting one certain model, (the *deterministic* inverse problem),
the course will also discuss how to proceed when there is uncertainty in which model to use.
The main focus will be on statistical methods, such as Bayesian inference, and how to 
efficiently compute predictions from distributions of models.

The models that will be used to illustrate methods in this class will be partial differential equations,
but the inverse problem techniques introduced will be much more widely applicable.

## Prerequisite Courses and Skills

CSE 6644 (Iterative Methods for Systems of Equations) or permission from the
instructor.

## Course Material

Theoretical topics: see calendar

Software tools introduced:

- [hIPPYlib](hippylib.github.io)
- [MUQ](muq.mit.edu)
- [dolfin-adjoint](www.dolfin-adjoint.org)
- [PETSc/TAO](https://www.mcs.anl.gov/petsc/)

In addition to lectures, there will be a *collaborative effort* between the
instructor and students in developing computational examples using the above
tools that demonstrate/test the methods and results in the theoretical topics.

## Assignments & Grading

90% of the final grade will be based on participation in in-class discussion and
contribution to the computational examples.

10% of the final grade will be based on a short final report.

**Grades** will be earned according to the standard scale:

|     |          |
| --- | ---      |
| A   | 90--100% |
| B   | 80--90%  |
| C   | 70--80%  |
| D   | 60--70%  |
| F   | 0--60%   |

## Academic Integrity

Georgia Tech aims to cultivate a community based on trust, academic integrity,
and honor. Students are expected to act according to the highest ethical
standards.  For information on Georgia Tech's Academic Honor Code, please visit
[http://www.catalog.gatech.edu/policies/honor-code/](http://www.catalog.gatech.edu/policies/honor-code/)
or [http://www.catalog.gatech.edu/rules/18/](http://www.catalog.gatech.edu/rules/18/).

## Accomodations for Individuals with Disabilities

If you are a student with learning needs that require special accommodation,
contact the Office of Disability Services at [(404)894-2563](tel:+14048942563)
or
[http://disabilityservices.gatech.edu/](http://disabilityservices.gatech.edu/),
as soon as possible, to make an appointment to discuss your special needs and
to obtain an accommodations letter.  Please also e-mail me as soon as possible
in order to set up a time to discuss your learning needs.


