
# Final Report

## Description:

Please write a short report on an computational inverse problem.  You may choose an example of your own,
one from the literature, or one from class materials.  If you are unsure of what to choose, I would recommend the example ["Inferring material properties of a cantilievered beam"](http://g2s3.com/labs/notebooks/EulerBernoulli_Lumped.html).

If you are choosing an example of your own, please send me your idea by **March 29** to see if it works.
(Generally, if your problem has non-trivial answers to most of the questions below, it should be fine.)

Topics to address in your report:

- What is the data in your problem?

  * Do you have multiple observations of the same phenomenon, or only one?

  * How accurate is the data?  Are there known sources of uncertainty?  How would
    you categorize those sources?

- What is your model of the process that generated the data?

  * What are the parameters of your model?  Which ones are known fully?  Which ones are unknown?  How would you categorize the uncertainty in the model parameters?

  * Are you able to compute with your model directly, or is computation
    necessarily approximate?  How well can we approximate the "true" model (can
    we develop models that "converge" to the true model?  In what sense do they
    converge, and at what rate?)?

  * Computationally, what does the process of "running" your model entail:
    is it an explicit simulation, or implicit?  Can you characterize
    the complexity of the model in terms of the number of parameters, number of observations,
    or the number of intermediate (e.g. "state") variables?

  * Is your model smooth?  If so, how do you compute the gradient of the parameter-to-observation
    map, and/or Hessian values?

- How is the inverse problem ill-posed? 

  * How can you quantify how ill-posed it is?
  (Can you compute/estimate the rank of a null-space?  A condition number?)  I

  * Is there model discrepancy?

- If you solve the inverse problem deterministically:

  * How do you deal with ill-posedness? (variational regularization as discussed in class, or some other approach?)

  * What is the nature of the solution: is it guaranteed to exist?  Is it guaranteed to be unique, or locally unique?

  * How do you solve the inverse problem computationally?  How efficient is your approach (can you describe its complexity)?

- If you solve the inverse problem statistically:

  * What is your statistical framework?  If it is Bayesian inference, how/why do you choose the prior that you did?

  * What values are you computing/estimating as a "solution" to the inverse problem? (The expectation of the parameters?  Their covariance?
    Statistics of a separate "quantity of interest" that depends on the parameters)?

  * How do you solve the inverse problem computationally?  How efficient is your approach?  How "good" is your solution (Can you bound
    the difference between the quantity you want to compute and what you actually computed?)



## Final Due Date: **May 2nd** (Last Day of Final Exams)

## Check-in:

Students should submit a draft **April 16** to get feedback by the final day of class **April 23**.

